import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  articles: Article[] = [];

  constructor() { }

  ngOnInit(): void {
    const articles = [
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
      {
        author: 'Mamy',
        img_url: '',
        date: Date.now(),
        title: 'Je ne support plus mes gosses',
        content: 'Alors voila hier les enfants mont retourner la maison avec le chien c\'était horrible',
        like: 0
      },
    ];
    this.articles = articles;
  }

}
