export interface Article {
    author: string;
    date: any;
    title: string;
    content: string;
    like: number;
}
